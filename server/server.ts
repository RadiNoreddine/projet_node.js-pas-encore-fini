import * as http from 'http';
import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as mongoose from 'mongoose';
import Tournament from './models/tournament';
import { TournamentsRouter } from './routes/tournaments.router';
import Member from './models/member';
import {MembersRouter} from './routes/member.router'
import * as jwt from 'jsonwebtoken';
import { AuthentificationRouter } from "./routes/authentication.router";

const MONGO_URL = 'mongodb://127.0.0.1/msn';

export class Server {
    private express: express.Application;
    private server: http.Server;
    private port: any;

    constructor() {
        this.express = express();
        this.middleware();
        this.mongoose();
        this.routes();
    }

    private middleware(): void {
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
    }

    // initialise les routes
    private routes() {
        this.express.use('/api/token', new AuthentificationRouter().router);
        this.express.use(AuthentificationRouter.checkAuthorization); 
        this.express.use('/api/members',new MembersRouter().router);
        this.express.use('/api/tournaments', new TournamentsRouter().router);
       
    }

    // initialise mongoose
    private mongoose() {
        (mongoose as any).Promise = global.Promise;     // see: https://stackoverflow.com/a/38833920
        let trials = 0;
        let connectWithRetry = () => {
            trials++;
            return mongoose.connect(MONGO_URL, err => {
                if (err) {
                    if (trials < 3) {
                        console.error('Failed to connect to mongo on startup - retrying in 2 sec');
                        setTimeout(connectWithRetry, 2000);
                    }
                    else {
                        console.error('Failed to connect to mongo after 3 trials ... abort!');
                        process.exit(-1);
                    }
                }
                else {
                    console.log('Connected to MONGODB');
                    this.initData();
                }
            });
        };
        connectWithRetry();
    }

    private initData() {
        
        Tournament.count({}).then(count => {
            if (count === 0) {
                console.log("Initializing data...");
                Tournament.insertMany([
                    { name: "courseDeLapin", start: "09-10-10", finish: "10-10-17" ,maxPlayers: 2 },
                    { name: "tests", start: "09-10-10", maxPlayers: 2 }
                    
                ]);
            }
        });
        Member.count({}).then(count=>{
            if(count===0){
                console.log("Initializing data ....");
                Member.insertMany([
                    {pseudo:"ben",password:"ben",profile:"Je m'apelle ben !"}
                ]);
            }
        });
    }


    // démarrage du serveur express
    public start(): void {
        this.port = process.env.PORT || 3000;
        this.express.set('port', this.port);
        this.server = http.createServer(this.express);
        this.server.listen(this.port, () => console.log(`Node/Express server running on localhost:${this.port}`));
    }
}
