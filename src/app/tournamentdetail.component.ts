import { Component, OnInit } from '@angular/core';
import { TournamentService, Tournament } from "app/tournament.service";
import {ActivatedRoute} from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
    selector: 'tournamentdetail',
    templateUrl: 'tournamentdetail.component.html',
   
    
   
    
})

export class TournamentDetail {
     public tournament:Tournament;
     tournamentID:string;
    
     constructor(private tournamentService: TournamentService,route: ActivatedRoute) { this.tournamentID = route.snapshot.params['name'];
     
    
    }

     ngOnInit() {
        this.tournamentService.getOne(this.tournamentID).subscribe(res => {
            this.tournament= res;
        })
    }
}