import { Component, OnInit } from '@angular/core';
import { TournamentService, Tournament } from "app/tournament.service";

@Component({
    selector: 'tournamentlist',
    templateUrl: 'tournamentlist.component.html'
})

export class TournamentListComponent implements OnInit {
    public tournaments:Tournament[];
    
    constructor(private tournamentService: TournamentService) { }

    ngOnInit() {
        this.tournamentService.getAll().subscribe(res => {
            this.tournaments = res;
        })
    }
}