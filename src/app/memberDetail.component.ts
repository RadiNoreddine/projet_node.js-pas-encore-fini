import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {DatePipe} from '@angular/common';
import { MemberService, Member } from "app/member.service";

@Component({
    selector: 'memberDetail',
    templateUrl: 'memberDetail.component.html'
                  
})


export class MemberDetail implements OnInit {
    public member: Member;
    public  memberName: string;
    
    constructor(private memberService: MemberService,route: ActivatedRoute) { 
        this.memberName=route.snapshot.params['id'];
        
    }

    ngOnInit() {
       
        this.memberService.getOne(this.memberName).subscribe(res => {
            this.member = res;    
        })
        
    }
}