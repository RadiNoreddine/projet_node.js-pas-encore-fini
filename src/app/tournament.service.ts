import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Observable";
import {Http, RequestOptions} from "@angular/http";
import {SecuredHttp} from "app/securedhttp.service";

import 'rxjs/add/operator/map';

export class Tournament{
    _id:string;
    name:string;
    start:Date;
    finish:Date;
    maxPlayers:Number

    constructor(data){
        this._id = data._id;
        this.name = data.name;
        this.start = data.start;
        this.finish = data.finish;
        this.maxPlayers = data.maxPlayers;
    }
}

const URL ='/api/tournaments';

@Injectable()
export class TournamentService {
    constructor(private http: SecuredHttp){

    }
    public getAll(): Observable<Tournament[]>{
        return this.http.get(URL)
            .map(result => {
                return result.json().map(json => new Tournament(json));
            });
    }
    public getOne(name : string): Observable<Tournament> {
        return this.http.get(URL +"/"+ name)
            .map(result => {
                let data = result.json();
                return data.length > 0 ? new Tournament(data[0]) : null;
            });
    }

}